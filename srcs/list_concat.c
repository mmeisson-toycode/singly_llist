
#include "_list.h"
#include "list.h"

s_list	*list_concat(s_list *first, s_list *second)
{
	s_list	*new = NULL;
	s_list	*tmp;

	FOR_LIST(first, node)
	{
		tmp = list_new(node->content, node->content_size);
		if (tmp == NULL)
		{
			return new;
		}
		list_push_top(&new, tmp);
	}
	FOR_LIST(second, node)
	{
		tmp = list_new(node->content, node->content_size);
		if (tmp == NULL)
		{
			return new;
		}
		list_push_top(&new, node);
	}
	list_reverse(&new);
	return new;
}

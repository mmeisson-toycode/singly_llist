
#include <stdint.h>

#include "_list.h"
#include "list.h"

static inline s_list	*merge_node(s_list *left, s_list *right, c_list_comparator callback)
{
	s_list		*new_head = NULL;
	s_list		*new_tail = NULL;
	s_list		*tmp = NULL;

	/* We do this first call in order to avoid to check if new_head is null in the loop */
	if (callback(left->content, left->content_size, right->content, right->content_size) <= 0)
	{
		tmp = left;
		left = left->next;
	}
	else
	{
		tmp = right;
		right = right->next;
	}
	new_head = new_tail = tmp;
	new_tail = tmp;

	while (left != NULL && right != NULL)
	{
		if (callback(left->content, left->content_size, right->content, right->content_size) <= 0)
		{
			tmp = left;
			left = left->next;
		}
		else
		{
			tmp = right;
			right = right->next;
		}
		new_tail->next = tmp;
		new_tail = tmp;
	}

	/* One of these is NULL, so it's like merging the end of list that's remain without branching */
	new_tail->next = (s_list *)((uintptr_t)left | (uintptr_t)right);
	return new_head;
}

s_list		*_list_msort(s_list *head, size_t length, c_list_comparator callback)
{
	s_list		*tmp = head;
	s_list		*left = head;
	s_list		*right;
	size_t		half_length;

	if (length < 2)
	{
		return head;
	}
	half_length = length / 2;
	for (size_t i = 1; i < half_length; i++)
	{
		tmp = tmp->next;
	}
	right = tmp->next;
	tmp->next = NULL;
	return merge_node(
		_list_msort(left, half_length, callback),
		_list_msort(right, length - half_length, callback),
		callback
	);
}

s_list		*list_msort(s_list **head, c_list_comparator callback)
{
	*head = _list_msort(*head, list_length(*head), callback);
	return *head;
}

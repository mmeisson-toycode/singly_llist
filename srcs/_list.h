#ifndef _LIST_H
# define _LIST_H

# include <stdlib.h>

# include "list.h"

/*
**	Internal define
*/
# ifndef MEM_ALIGN
#  define MEM_ALIGN	16
# endif


struct	s_list
{
	struct s_list	*next;
	size_t		content_size;
	void const	*content;
};

#endif

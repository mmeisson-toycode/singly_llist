
#include "_list.h"
#include "list.h"

void	*list_find_ctx(s_list *head, c_list_validator_ctx callback, void *context)
{
	FOR_LIST(head, node)
	{
		if (callback(node->content, node->content_size, context) != VALIDATOR_NOK)
		{
			return (void *)node->content;
		}
	}
	return NULL;
}

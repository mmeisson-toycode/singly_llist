
#include "list.h"
#include "_list.h"

void		list_push_bot(s_list **head, s_list *node)
{
	s_list	*iterator;

	if (*head != NULL)
	{
		iterator = *head;
		while (iterator->next != NULL)
		{
			iterator = iterator->next;
		}
		iterator->next = node;
	}
	else
	{
		*head = node;
	}
}


#include "_list.h"
#include "list.h"

void	*list_find(s_list *head, c_list_validator callback)
{
	FOR_LIST(head, node)
	{
		if (callback(node->content, node->content_size) != VALIDATOR_NOK)
		{
			return (void *)node->content;
		}
	}
	return NULL;
}

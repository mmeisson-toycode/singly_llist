
#include "list.h"
#include "_list.h"

void	*list_pop_bot(s_list **head)
{
	void	*mem;
	s_list	*iter;
	s_list	*prev;

	if (*head == NULL)
	{
		return NULL;
	}
	prev = *head;
	if (prev->next == NULL)
	{
		mem = (void *)prev->content;
		*head = NULL;
	}
	else
	{
		iter = prev->next;
		while (iter->next != NULL)
		{
			prev = iter;
			iter = iter->next;
		}
		prev->next = NULL;
		mem = (void *)prev->content;
	}
	return mem;
}

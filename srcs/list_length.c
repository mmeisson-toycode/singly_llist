
#include "list.h"
#include "_list.h"

size_t      list_length(s_list *head)
{
	size_t	length = 0;

	while (head != NULL)
	{
		length++;
		head = head->next;
	}
	return length;
}
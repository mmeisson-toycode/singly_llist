
#include "list.h"
#include "_list.h"

void		list_push_top(s_list **head, s_list *node)
{
	node->next = *head;
	*head = node;
}

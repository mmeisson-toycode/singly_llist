
#include "list.h"
#include "_list.h"

void	list_reduce_ctx(s_list *head, c_list_reducor_ctx callback, void **data, void * context)
{
	FOR_LIST(head, node)
	{
		callback((void *)node->content, node->content_size, data, context);
	}
}


#include "_list.h"
#include "list.h"

void	list_iter_ctx(s_list *head, c_list_iterator_ctx callback, void *context)
{
	FOR_LIST(head, node)
	{
		callback((void *)node->content, node->content_size, context);
	}
}


#include "_list.h"
#include "list.h"

void	list_iter(s_list *head, c_list_iterator callback)
{
	FOR_LIST(head, node)
	{
		callback((void *)node->content, node->content_size);
	}
}

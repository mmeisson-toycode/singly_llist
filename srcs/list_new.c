
#include <stdlib.h>
#include <string.h>

#include "list.h"
#include "_list.h"

#define ROUND_UP(size, multiple) (size % multiple ? (size / multiple + 1) * multiple : size)


/*
**	Create a new node, allocating at once space for data and node
*/
s_list		*list_new(void const * const data, size_t data_length)
{
	void		*mem;
	s_list		*node;
	size_t		rounded_length;


	rounded_length = ROUND_UP(data_length, MEM_ALIGN);

	mem = malloc(rounded_length + sizeof *node);
	if (mem == NULL)
	{
		return NULL;
	}
	node = mem + rounded_length;
	node->next = 0;
	node->content = mem;
	node->content_size = data_length;
	memcpy((void *)node->content, data, data_length);
	return	node;
}


#include <assert.h>

#include "list.h"
#include "_list.h"

s_list		*list_map_ctx(s_list *list, c_list_creator_ctx callback, void *context)
{
	s_list	*mapped;
	s_list	*new_node;

	mapped = NULL;
	FOR_LIST(list, node)
	{
		new_node = callback(node->content, node->content_size, context);
		list_push_top(&mapped, new_node);
	}
	if (mapped != NULL)
	{
		list_reverse(&mapped);
	}
	return mapped;
}


#include <assert.h>

#include "_list.h"
#include "list.h"

s_list		*list_filter(s_list *list, c_list_validator callback)
{
	s_list	*filtered;
	s_list	*new_node;

	filtered = NULL;
	FOR_LIST(list, node)
	{
		if (callback(node->content, node->content_size) != VALIDATOR_NOK)
		{
			new_node = list_new(node->content, node->content_size);
			assert(new_node != NULL);
			list_push_top(&filtered, new_node);
		}
	}
	if (filtered != NULL & filtered->next != NULL)
	{
		list_reverse(&filtered);
	}
	return filtered;
}

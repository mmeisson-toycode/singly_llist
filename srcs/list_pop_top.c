
#include "list.h"
#include "_list.h"

void	*list_pop_top(s_list **head)
{
	register void	*mem;

	if (*head != NULL)
	{
		mem = (void *)(*head)->content;
		*head = (*head)->next;
	}
	else
	{
		mem = NULL;
	}
	return mem;
}


#include <string.h>

#include "list.h"
#include "_list.h"

s_list      *list_qsort(s_list **head, c_list_qcomparator callback)
{
    size_t      length = list_length(*head);

    void    *data = malloc(sizeof((*head)->content_size) * length);
    void    *curr_data;

    curr_data = data;
    for (s_list *iter = *head; iter != NULL; iter = iter->next)
    {
        memcpy(curr_data, iter->content, iter->content_size);
        curr_data += iter->content_size;
    }
    qsort(data, length, sizeof((*head)->content_size), callback);

    curr_data = data;
    for (s_list *iter = *head; iter != NULL; iter = iter->next)
    {
        memcpy((void *)iter->content, curr_data, iter->content_size);
        curr_data += iter->content_size;
    }
    return *head;
}

#include "list.h"
#include "_list.h"

static s_list		*_list_reverse(s_list *node)
{
	struct s_list	*next;
	struct s_list	*prev;

	prev = node;
	node = node->next;
	prev->next = NULL;
	while (node->next != NULL)
	{
		next = node->next;
		node->next = prev;
		prev = node;
		node = next;
	}
	node->next = prev;
	return node;
}

s_list		*list_reverse(s_list **head)
{
	if (*head == NULL)
	{
		return NULL;
	}
	if ((*head)->next == NULL)
	{
		return *head;
	}
	*head = _list_reverse(*head);
	return *head;
}

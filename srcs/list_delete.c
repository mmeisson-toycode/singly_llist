
#include "list.h"
#include "_list.h"

void	list_delete(s_list **head, c_list_deletor callback)
{
	s_list	*prev;

	prev = *head;
	if (prev == NULL)
	{
		return;
	}

	FOR_LIST(prev->next, node)
	{
		callback((void *)prev->content, prev->content_size);
		/* prev should actually be invalid */
		prev = node;
	}
	callback((void *)prev->content, prev->content_size);
	*head = NULL;
}

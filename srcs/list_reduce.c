
#include "list.h"
#include "_list.h"

void	list_reduce(s_list *head, c_list_reducor callback, void **data)
{
	FOR_LIST(head, node)
	{
		callback((void *)node->content, node->content_size, data);
	}
}

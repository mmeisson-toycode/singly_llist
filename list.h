#ifndef LIST_H
# define LIST_H

# include <stdlib.h>

# define VALIDATOR_NOK		0
# define VALIDATOR_OK		(!VALIDATOR_NOK)

# define	FOR_LIST(list, iterator) for (s_list *iterator = list; iterator != NULL; iterator = iterator->next)

struct s_list;
typedef struct s_list	s_list;

typedef void	(*c_list_iterator)(void *content, size_t content_size);
typedef void	(*c_list_iterator_ctx)(void *content, size_t content_size, void *context);
typedef void	(*c_list_deletor)(void *content, size_t content_size);
typedef int		(*c_list_validator)(void const * const content, size_t content_size);
typedef int		(*c_list_validator_ctx)(void const * const content, size_t content_size, void *context);
typedef void	(*c_list_reducor)(void const * const content, size_t content_size, void **data);
typedef void	(*c_list_reducor_ctx)(void const * const content, size_t content_size, void **data, void *context);
typedef s_list	*(*c_list_creator)(void const * const content, size_t content_size);
typedef s_list	*(*c_list_creator_ctx)(void const * const content, size_t content_size, void *context);
typedef int		(*c_list_comparator)(void const * const left_content, size_t left_size, void const * const right_content, size_t right_size);
typedef int		(*c_list_qcomparator)(const void *left, const void *right);


s_list		*list_new(void const * const data, size_t data_length);

size_t		list_length(s_list *head);

void		list_push_top(s_list **head, s_list *node);
void		list_push_bot(s_list **head, s_list *node);

/*
**	Please note that list_pop_* remove the data from list
**	You will then be responsible to free data after usage
*/
void		*list_pop_top(s_list **head);
void		*list_pop_bot(s_list **head);

void		list_iter(s_list *lst, c_list_iterator callback);
void		*list_find(s_list *lst, c_list_validator callback);

void		list_iter_ctx(s_list *lst, c_list_iterator_ctx callback, void *context);
void		*list_find_ctx(s_list *lst, c_list_validator_ctx callback, void *context);

s_list		*list_concat(s_list *first, s_list *second);
s_list		*list_reverse(s_list **list);

s_list		*list_filter(s_list *list, c_list_validator callback);
s_list		*list_map(s_list *list, c_list_creator callback);
void		list_reduce(s_list *list, c_list_reducor callback, void **data);

s_list		*list_filter_ctx(s_list *list, c_list_validator_ctx callback, void *context);
s_list		*list_map_ctx(s_list *list, c_list_creator_ctx callback, void *context);
void		list_reduce_ctx(s_list *list, c_list_reducor_ctx callback, void **data, void *context);

/* In place, merge sort */
s_list      *list_msort(s_list **head, c_list_comparator);

/*
**  Qsort. Data is stored in an array, then libC's qsort is called
**  Use this if memory is not a concern and if every node has data of same size  
*/
s_list      *list_qsort(s_list **head, c_list_qcomparator);

/*
**	Please note that callback is responsible to call free on the content
**	Not freeing content in callback could lead to memory leak
*/
void		list_delete(s_list **head, c_list_deletor callback);

/*
**	Some overridable shortcuts
*/

# ifndef list_push
#  define list_push(head, node) list_push_top(head, node)
# endif

# ifndef list_pop
#  define list_pop(head) list_pop_top(head)
# endif

# ifndef list_sort
#  define list_sort(head, callback) list_msort(head, callback)
# endif

/*
**	Wrapp list's functionnalities to build a stack interface
**	The last data pushed in the stack will be the first to come
*/

typedef s_list		s_stack;

# define stack_new(data, data_length) list_new(data, data_length)
# define stack_push(data, data_length) list_push_top(data, data_length)
# define stack_pop(head) list_pop_top(head, data_length)



/*
**	Wrapp list's functionnalities to build a queue interface
**	The first data pushed in the stack will be the first to come
*/

typedef s_list		s_queue;

# define queue_new(data, data_length) list_new(data, data_length)
# define queue_push(data, data_length) list_push_top(data, data_length)
# define queue_pop(head) list_pop_bot(head, data_length)

#endif


NAME		= liblist

VPATH		= ./srcs

SRCS		= list_delete.c
SRCS		+= list_filter.c
SRCS		+= list_find.c
SRCS		+= list_iter.c
SRCS		+= list_map.c
SRCS		+= list_new.c
SRCS		+= list_pop_bot.c
SRCS		+= list_pop_top.c
SRCS		+= list_push_bot.c
SRCS		+= list_push_top.c
SRCS		+= list_reduce.c
SRCS		+= list_reverse.c
SRCS		+= list_length.c
SRCS		+= list_msort.c
SRCS		+= list_qsort.c
SRCS		+= list_filter_ctx.c
SRCS		+= list_find_ctx.c
SRCS		+= list_iter_ctx.c
SRCS		+= list_map_ctx.c
SRCS		+= list_reduce_ctx.c
SRCS		+= list_concat.c


OBJS_PATH	= ./.objs/
OBJS_NAMES	= $(SRCS:.c=.o)

INC_PATH	= ./srcs ./

CFLAGS		= -Wall -Werror -Wextra -Ofast -MD

OBJS		= $(addprefix $(OBJS_PATH),$(OBJS_NAMES))
INC		= $(addprefix -I,$(INC_PATH))

LDFLAGS		=

DEPS		= $(OBJS:.o=.d)



all: $(NAME)

$(NAME): static

static: $(OBJS)
	ar rc $(NAME).a $(OBJS)

dynamic: $(OBJS)
	$(CC) -Wl,-fPIC -shared $(OBJS) $(LDFLAGS) -o $(NAME).so

$(OBJS_PATH)%.o: %.c Makefile
	@mkdir -p $(OBJS_PATH)
	$(CC) $(CFLAGS) $(INC) -o $@ -c $<

clean:
	rm -rf $(OBJS_PATH)

fclean: clean
	rm -f $(NAME).a $(NAME).so

re: fclean all

-include $(DEPS)
